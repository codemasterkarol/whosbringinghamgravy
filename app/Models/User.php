<?php

/**
 * Created by PhpStorm.
 * User: karolbrennan
 * Date: 10/24/15
 * Time: 8:11 PM
 */
namespace Models;

use Helpers\Database;
use Helpers\Session;

/**
 * Class User
 * @package Models
 */
class User extends \Core\Model
{
    /**
     * @var int the regular user role
     */
    const ROLE_USER = 1;

    /**
     * User's name
     * @var string
     */
    public $name;

    /**
     * User's email
     * @var string
     */
    public $email;

    /**
     * User's role
     * @var int
     */
    public $role;

    /**
     * User's id
     * @var int
     */
    public $id;

    /**
     * User's hashed password
     * @var string
     */
    protected $password;

    /**
     * Create new user object based on passed variables
     *
     * @param $data array the user info to build from
     */
    public function __construct($data) {
        if(isset($data['name'])) {
            $this->name = $data['name'];
        }
        if(isset($data['email'])) {
            $this->email = $data['email'];
        }
        if(isset($data['password'])) {
            $this->password = $data['password'];
        }
        if(isset($data['role'])) {
            $this->role = $data['role'];
        }
        if(isset($data['id'])) {
            $this->id = $data['id'];
        }

        parent::__construct();
    }

    public static function doesEmailExists($data)
    {
        $db = Database::get();
        return !empty($db->select("select id from users where email = :email", array(':email' => $data)));
    }

    /**
     * Saves this model
     */
    public function save(){
        $this->db->insert('users', [
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password
        ]);
    }

    /**
     * Returns the authenticated user OR false if can't log in
     * @return int|false
     */
    public function login(){
        $login = false;
        $result = $this->db->select("select id from users where email = :email and password = :password",
            array(':email' => $this->email, ':password' => $this->password));

        if (!empty($result[0])) {
            $login = $result[0]->id;
        }

        return $login;
    }

    /**
     * Hashes password according to our way of doing it
     *
     * @param $password
     * @return string
     */
    public static function hashPassword($password)
    {
        return sha1($password);
    }
}