<?php
/**
 * Home Page Language
 * @author Karol Brennan
 * @date Oct 23 2015
 */
return array(

	// Index method
	'home_text' => 'Welcome',
	'home_message' => '
		Will you be bringing the Ham Gravy?
	',

	// Buttons
	'main_callout' => 'Sign Up!'

);
