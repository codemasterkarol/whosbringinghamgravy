<?php
/**
 * Footer Layout
 * @author Karol Brennan
 * @date Oct 23 2015
 */

use Helpers\Assets;
use Helpers\Url;
use Helpers\Hooks;

//initialise hooks
$hooks = Hooks::get();
?>

			</div>
		</div>
	</div>
</section>

<footer class="footer">
	<div class="wrapper">
		<div class="row">
			<div class="col c50">
				<p><strong>Who's Bringing Ham Gravy</strong></p>
			</div>
			<div class="col c50 right">
                <p>&copy; 2015 <a href="http://karolbrennan.com" title="Karol Brennan">Karol Brennan</a></p>
            </div>
		</div>
	</div>
</footer>

<!-- JS -->
<?php
Assets::js(array(
	Url::templatePath() . 'js/jquery.js'
));

//hook for plugging in javascript
$hooks->run('js');

//hook for plugging in code into the footer
$hooks->run('footer');
?>

</body>
</html>
