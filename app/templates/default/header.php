<?php
/**
 * Header layout
 * @author Karol Brennan
 * @date Oct 23 2015
 */

use Helpers\Assets;
use Helpers\Url;
use Helpers\Hooks;

//initialise hooks
$hooks = Hooks::get();
?>
<!DOCTYPE html>
<html lang="<?php echo LANGUAGE_CODE; ?>">
<head>

	<!-- Site meta -->
	<meta charset="utf-8">
	<?php
	//hook for plugging in meta tags
	$hooks->run('meta');
	?>
	<title><?php echo $data['title'].' - '.SITETITLE; ?></title>

	<!-- CSS -->
	<?php
	Assets::css(array(
		'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700',
		Url::templatePath() . 'css/style.css',
	));

	//hook for plugging in css
	$hooks->run('css');
	?>

</head>
<body>
<?php
//hook for running code after body tag
$hooks->run('afterBody');
?>

<header class="header">
	<div class="wrapper">
		<div class="row">
			<div class="c50 col"><a href="/"><h1>Who's Bringing Ham Gravy?</h1></a></div>
			<div class="c50 col right">
				<nav class="navigation">
					<ul class="menu">
						<?php include('menu.php'); ?>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>

<section class="main">
	<div class="wrapper">
		<div class="row"><div class="col c100">
