<?php
/**
 * Created by PhpStorm.
 * User: karolbrennan
 * Date: 10/24/15
 * Time: 12:34 AM
 */
    $loggedin = (\Helpers\Session::get('loggedin') ? true : false);
?>

<li><a href="/">Home</a></li>
<li><a href="<?= ($loggedin ? '/dashboard' : '/signup'); ?>"><?= ($loggedin ? 'Dashboard' : 'Sign Up'); ?></a></li>
<li><a href="<?= ($loggedin ? '/logout' : '/login') ?>"><?= ($loggedin ? 'Logout' : 'Login'); ?></a></li>
