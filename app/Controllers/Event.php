<?php
/**
 * Created by PhpStorm.
 * User: karolbrennan
 * Date: 10/25/15
 * Time: 2:39 PM
 */

namespace Controllers;

use Core\Controller;
use Core\View;
use Helpers\Session;
use Helpers\Url;

class Event extends Controller
{
    /**
     * Display all current events
     */
    public function displayAllEvents(){

    }

    /**
     * Display user's events
     */
    public function displayUserEvents(){

    }

    /**
     * Display event creation page
     */
    public function create(){
        // make sure user is logged in
        if (empty(Session::get('current-user-id'))) {
            Url::redirect('login');
        };

        $data['title'] = 'Create an event!';

        View::renderTemplate('header', $data);
        View::render('event/create', $data);
        View::renderTemplate('footer', $data);
    }

}