<?php
/**
 * Home controller
 *
 * @author Karol Brennan
 * @version 1.0
 * @date October 23, 2015
 * @date updated October 23, 2015
 */

namespace Controllers;
use Core\View;
use Core\Controller;
use Helpers\Session;
use Helpers\Url;

/**
 * Home controller, handles main index page
 */
class Home extends Controller
{
    /**
     * Define Index page title and load template files
     */
    public function index()
    {
        $data['title'] = 'Home';
        $data['logged-in'] = Session::get('current-user-id');
        View::renderTemplate('header', $data);
        View::render('home/index', $data);
        View::renderTemplate('footer', $data);
    }

    /**
     * Define dashboard title and load template files
     */
    public function dashboard(){
        if(!(Session::get('loggedin'))) {Url::redirect('login');};
        $data['title'] = 'Dashboard';

        View::renderTemplate('header',$data);
        View::render('home/dashboard',$data);
        View::renderTemplate('footer',$data);
    }
}
