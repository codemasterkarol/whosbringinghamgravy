<?php
/**
 * Created by PhpStorm.
 * User: karolbrennan
 * Date: 10/24/15
 * Time: 5:20 PM
 */

namespace Controllers;
use Core\View;
use Core\Controller;
use Helpers\Session;
use Helpers\Url;
use Models\User;

class Signup extends Controller
{
    /**
     * Call the parent construct
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Define signup page title and load template files
     */
    public function index()
    {
        $sessionFormErrors = (array) Session::get('signupErrors');
        $signupFormErrors = [
            'name' => !empty($sessionFormErrors['name']) ? $sessionFormErrors['name'] : '',
            'email' => !empty($sessionFormErrors['email']) ? $sessionFormErrors['email'] : '',
            'password' => !empty($sessionFormErrors['password']) ? $sessionFormErrors['password'] : ''
        ];

        $sessionFormValues = (array) Session::get('signupValues');
        $signupFormValues = [
            'name' => !empty($sessionFormValues['name']) ? $sessionFormValues['name'] : '',
            'email' => !empty($sessionFormValues['email']) ? $sessionFormValues['email'] : ''
        ];

        $data['title'] = 'Sign Up';
        $data['signupFormErrors'] = $signupFormErrors;
        $data['signupFormValues'] = $signupFormValues;

        View::renderTemplate('header', $data);
        View::render('signup/index', $data);
        View::renderTemplate('footer', $data);
    }

    /**
     * Signup Process
     *
     * Gets input values from signup form and validates those values
     * Assigns values or errors to an array
     */
    public function signupProcess()
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST') {
            $formValues = $formErrors = array();

            if(!($formValues['name'] = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING))){
                $formErrors['name'] = "What's your name? Is it Mary, or Sue?";
            }
            if(!($formValues['email'] = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL))){
                $formErrors['email'] = "Give us your email, we won't spam! Promise!";
            }
            if(!($password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING))){
                $formErrors['password'] = "Pick a password, yo!";
            }

            if(empty($formErrors)){
                if(User::checkIfUserEmailExists($formValues['email'])) {
                    $formErrors['email'] = "Sorry, this email already exists!
                    Please choose a different one, or <a href='/login'>login</a>.";
                }
            }

            if(!empty($formErrors)){
                Session::set('signupErrors', $formErrors);
                Session::set('signupValues', $formValues);
                Url::redirect('signup');
            }

            Session::destroy('signupValues');
            Session::destroy('signupErrors');

            $formValues['password'] = User::hashPassword($password);
            $user = new User($formValues);
            $user->save();

            Session::set('loggedin', true);
            Url::redirect('');
        }
    }

}