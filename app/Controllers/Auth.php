<?php
/**
 * Created by PhpStorm.
 * User: karolbrennan
 * Date: 10/24/15
 * Time: 5:20 PM
 */

namespace Controllers;
use Core\View;
use Core\Controller;
use Helpers\Session;
use Helpers\Url;
use Models\User;

class Auth extends Controller
{
    /**
     * Define login page title and load template files
     */
    public function loginPage()
    {
        $data['title'] = 'Login';
        $data['loginFormErrors'] = (array) Session::get('loginFormErrors');
        $data['loginFormValues'] = (array) Session::get('loginFormValues');

        View::renderTemplate('header', $data);
        View::render('auth/index', $data);
        View::renderTemplate('footer', $data);
    }

    /**
     * Login controller
     */
    public function loginProcess() {
        if($_SERVER['REQUEST_METHOD'] === 'POST') {
            $formValues = $formErrors = array();

            if(!($formValues['email'] = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL))){
                $formErrors['email'] = "Please enter a valid email.";
            }
            if(!($password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING))){
                $formErrors['password'] = "Input your password, yo!";
            }

            if(!$formErrors){
                if(User::doesEmailExists($formValues['email'])){
                    $formErrors['login'] = "Sorry, there is no user registered with this email address.
                                            Please try again or <a href='/signup'>signup</a>.";
                } else {
                    $formValues['password'] = User::hashPassword($password);
                    $user = new User($formValues);
                    if ($userId = $user->login()) {
                        Session::set('current-user-id', $userId);
                        Url::redirect('dashboard');
                    }
                }
            }

            Session::set('loginFormErrors', $formErrors);
            Session::set('loginFormValues', $formValues);
            Url::redirect('login');
        }
        else {
            Url::redirect('login');
        }
    }

    /**
     * Logout controller
     */

    public function logout(){
        Session::destroy();
        Url::redirect('');
    }
}