<?php
/**
 * Config
 *
 * @author Karol Brennan
 * @version 1.0
 * @date October 23, 2015
 * @date updated
 */

namespace Core;

use Helpers\Session;

/**
 * Configuration constants and options.
 */
class Config
{
    /**
     * Executed as soon as the framework runs.
     */
    public function __construct()
    {
        /**
         * Turn on output buffering.
         */
        ob_start();

        /**
         * Define relative base path.
         */
        $environment = getenv("APPLICATION_ENV");
        if($environment === "development") {
            define('DIR', 'http://ham.dev/');
        } else {
            define('DIR', 'http://whosbringinghamgravy.com/');
        }

        /**
         * Set default controller and method for legacy calls.
         */
        define('DEFAULT_CONTROLLER', 'home');
        define('DEFAULT_METHOD', 'index');

        /**
         * Set the default template.
         */
        define('TEMPLATE', 'default');

        /**
         * Set a default language.
         */
        define('LANGUAGE_CODE', 'en');

        //database details ONLY NEEDED IF USING A DATABASE

        /**
         * Database engine default is mysql.
         */
        define('DB_TYPE', 'mysql');

        /**
         * Database details
         */

        require('credentials.php');

        /**
         * PREFER to be used in database calls default is smvc_
         */
        define('PREFIX', 'smvc_');

        /**
         * Set prefix for sessions.
         */
        define('SESSION_PREFIX', 'smvc_');

        /**
         * Optional create a constant for the name of the site.
         */
        define('SITETITLE', 'Who\'s Bringing Ham Gravy?');

        /**
         * Optionall set a site email address.
         */
        define('SITEEMAIL', 'codemasterkarol@gmail.com');

        /**
         * Turn on custom error handling.
         */
        set_exception_handler('Core\Logger::ExceptionHandler');
        set_error_handler('Core\Logger::ErrorHandler');

        /**
         * Set timezone.
         */
        date_default_timezone_set('Europe/London');

        /**
         * Start sessions.
         */
        Session::init();
    }
}
