<?php
/**
 * Routes - all standard routes are defined here.
 *
 * @author Karol Brennan
 * @version 1.0
 * @date updated Oct 24, 2015
 */

/** Create alias for Router. */
use Core\Router;
use Helpers\Hooks;

/** Define routes. */
Router::any('','Controllers\Home@index');
Router::any('signup','Controllers\Signup@index');
Router::any('signup/process','Controllers\Signup@signupProcess');
Router::any('login','Controllers\Auth@loginPage');
Router::any('login/process','Controllers\Auth@loginProcess');
Router::any('logout','Controllers\Auth@logout');
Router::any('dashboard','Controllers\Home@dashboard');
Router::any('events','Controllers\Event@listing');
Router::any('event/create','Controllers\Event@create');
Router::any('event/edit','Controllers\Event@edit');

/** Module routes. */
$hooks = Hooks::get();
$hooks->run('routes');

/** If no route found. */
Router::error('Core\Error@index');

/** Turn on old style routing. */
Router::$fallback = false;

/** Execute matched routes. */
Router::dispatch();