<?php
/**
 * Created by PhpStorm.
 * User: karolbrennan
 * Date: 10/23/15
 * Time: 11:27 PM
 */
?>

<?php if (!$data['logged-in']) { ?>
    <p class="callout">Will you be bringing the Ham Gravy?</p>
    <p class="callout"><a href="/signup" class="callout-btn">Sign Up!</a></p>
<?php } else { ?>
    <p class="callout">Welcome, Your Name</p>
    <p class="callout"><a href="/event/create" class="callout-btn">Create an Event!</a></p>
<?php } ?>
