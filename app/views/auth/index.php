<?php
/**
 * Created by PhpStorm.
 * User: karolbrennan
 * Date: 10/24/15
 * Time: 11:35 PM
 */

$emailValue = !empty($data['loginFormValues']['email']) ? $data['loginFormValues']['email'] : '';
?>

<h2>Login</h2>

<?php
if (!empty($data['loginFormErrors']['login'])) {
    echo "<p><span class='error'>{$data['loginFormErrors']['login']}</span></p>";
}
?>

<form id="login" method="post" action="/login/process">
    <p>
        <label>Email:
            <input type="email" name="email" placeholder="everyoneuses@gmail.com" value="<?= $emailValue ?>" required>
        </label>
        <?php
        if (!empty($data['loginFormErrors']['email'])) {
            echo "<span class='error'>{$data['loginFormErrors']['email']}</span>";
        }
        ?>
    </p>
    <p>
        <label>Password:
            <input type="password" name="password" required>
        </label>
        <?php
        if (!empty($data['loginFormErrors']['password'])) {
            echo "<span class='error'>{$data['loginFormErrors']['password']}</span>";
        }
        ?>
    <p>
        <input type="submit" id="submit" value="Login!">
    </p>
</form>
