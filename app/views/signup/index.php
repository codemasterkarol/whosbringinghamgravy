<?php
/**
 * Created by PhpStorm.
 * User: karolbrennan
 * Date: 10/24/15
 * Time: 12:41 AM
 */
?>

<h2>Will you bring the Ham Gravy?</h2>

<form novalidate id="signup" method="post" action="/signup/process">
    <p><label>Name:  <input type="text" name="name" placeholder="Chris P. Bacon"
                            value="<?= $data['signupFormValues']['name'] ?>" required></label>
    <?= "<span class='error'>" . $data['signupFormErrors']['name'] . "</span>" ?></p>

    <p><label>Email: <input type="email" name="email" id="email" placeholder="everyoneuses@gmail.com"
                            value="<?= $data['signupFormValues']['email'] ?>" required></label>
    <?= "<span class='error'>" . $data['signupFormErrors']['email'] . "</span>" ?></p>

    <p><label>Password: <input type="password" name="password" id="password"
                            placeholder="ilovehamgravy!" required></label>
    <?= "<span class='error'>" . $data['signupFormErrors']['password'] . "</span>" ?></p>

    <p><input type="submit" id="submit" value="Sign Up!"></p>
</form>