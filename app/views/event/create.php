<?php
/**
 * Created by PhpStorm.
 * User: karolbrennan
 * Date: 10/25/15
 * Time: 1:48 PM
 */

?>


<h2>Create a new event!</h2>

<form novalidate id="new-event" method="post" action="/event/create">

    <div class="row">
        <div class="c50 col">
            <label>Event Type:
                <select name="type" >
                    <option value="1">Potluck</option>
                    <option value="2">New Year's Day</option>
                    <option value="3">Valentine's Day</option>
                    <option value="4">St. Patrick's Day</option>
                    <option value="5">Easter</option>
                    <option value="6">Cinco de Mayo</option>
                    <option value="7">Mother's Day</option>
                    <option value="8">Memorial Day</option>
                    <option value="9">Father's Day</option>
                    <option value="10">Independence Day</option>
                    <option value="11">Parent's Day</option>
                    <option value="12">Labor Day</option>
                    <option value="13">Halloween</option>
                    <option value="14">Veteran's Day</option>
                    <option value="15">Thanksgiving</option>
                    <option value="16">Christmas</option>
                    <option value="17">Other Holiday</option>
                </select>
            </label>
        </div>
        <div class="c50 col">
            <label>Event Title: <input type="text" name="title" placeholder="Turkey Day!" required></label>
        </div>
    </div>

    <div class="row">
        <div class="c50 col">
            <label>Date: <input type="date" name="event_date" value="
                <?= (!empty($data['createEventFormData']['date']) ? $data['createEventFormData'] : date("Y-m-d"));?>"
                required></label>
        </div>
        <div class="c50 col">
            <label>Time: <input type="time" name="time" id="time" value="14:00:00" required></label>
        </div>
    </div>

    <div class="row">
        <div class="c100 col">
            <span class="block"><label>Description: </span>
            <textarea name="description" placeholder="Describe your event!"></textarea></label>
        </div>
    </div>

    <div class="row">
        <div class="c100 col">
            <label>Location: <input type="text" name="location" placeholder="The Brennan House!"></label>
        </div>
    </div>

    <div class="row">
        <div class="c50 col">
            <label>Address: <input type="text" name="address1" placeholder="1234 Main St."></label>
        </div>
        <div class="c50 col">
            <label>Address (cont): <input type="text" name="address2" placeholder="PO Box 321"></label>
        </div>
    </div>

    <div class="row">
        <div class="c33 col">
            <label>City: <input type="text" name="city"></label>
        </div>
        <div class="c33 col">
            <label>State:
                <select name="state">
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI" selected>Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>
            </label>
        </div>
        <div class="c33 col">
            <label for="zip">Zip: <input type="number" name="zip"></label>
        </div>
    </div>

    <!-- @todo set up phone fields to auto format with hyphens KB-->
    <div class="row">
        <div class="c50 col">
            <label for="phone1">Contact Phone: <input type="number" name="phone1" placeholder="262-555-5555"></label>
        </div>
        <div class="c50 col">
            <label for="phone2">Alt. Phone: <input type="number" name="phone2" placeholder="262-555-5555"></label>
        </div>
    </div>

    <div class="row">
        <div class="c100 col center">
            <input type="submit" id="submit" value="Create Event">
        </div>
    </div>
</form>
